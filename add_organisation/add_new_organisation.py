import unittest
import time
import random

from globals import global_settings as globals
from helpers import operational_helpers as oh, functional_helpers as fh
from helpers.wrappers import screenshot_decorator
from selenium.webdriver.common.keys import Keys


class AddOrganisation(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.ef_driver = globals.init_driver()
        self.base_url = globals.base_url
        self.unique_id = str(time.time())[:10]
        self.pages = globals.Pages(self.ef_driver)
        self.user_email = f'karina.b+{self.unique_id}@weblinksoftware.com'
        self.foo_full_name = ['Polskie Koleje', 'Chińskie Koleje', 'Hyland', 'Sii', 'Future Processing', 'Shiji',
                         'Kodak', 'WodKan', 'Weblink', 'Nowy Dom', 'TestoNeo',
                         'ING Bank', 'Bombardier', 'Wodociągi']
        self.foo_short_name = ['PK', 'ChK', 'H', 'Sii', "FP", 'Shiji', 'Kod', 'WK',
                             'Web', 'NDy', 'TN', 'ING', 'Bomba']
        self.randoms__name = random.choice(self.foo_full_name)
        self.full_name = f'{self.randoms__name}{self.unique_id}'
        self.randoms_short_name = random.choice(self.foo_short_name)


    @classmethod
    def tearDownClass(self):
        self.ef_driver.quit()

    @screenshot_decorator
    def test_01_login_to_page(self):
        expected_title = 'Weblink StartApp'
        driver = self.ef_driver
        driver.maximize_window()

        fh.assert_title(driver, expected_title)

    @screenshot_decorator
    def test_02_go_to_org_list(self):
        header_title_xpath = '//*[@id="container-3"]/content/app-organisation-list/div/div/div[1]/h2'
        expected_title = 'Organisation List'
        driver = self.ef_driver

        self.pages.org.go_to_organisation_list()
        fh.assert_element_text(driver, header_title_xpath, expected_title)

    @screenshot_decorator
    def test_03_add_new_org(self):
        address = 'Warszawa, ul Jodłowa 456 '
        vat_number = '345 354'
        company_vat_tax_rate = '23'
        search_table_input_xpath = '//*[@id="mat-input-1"]'
        new_org_xpath = f'//*[contains(text(), "{self.randoms_short_name }")]'
        driver = self.ef_driver

        self.pages.org.add_new_organisation(self.full_name, self.foo_short_name, address, vat_number, company_vat_tax_rate)

        search_table_input = oh.visibility_of_element_wait(driver, search_table_input_xpath, 100)
        search_table_input.send_keys(self.full_name)
        search_table_input.send_keys(Keys.ENTER)
        time.sleep(8)
        fh.found_items_in_list(driver, new_org_xpath)