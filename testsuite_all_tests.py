import unittest
from add_organisation.add_new_organisation import AddOrganisation
from add_user.login_to_app import LoginToApp
from add_user.add_new_user import AddUser

def full_suite():

    test_suite = unittest.TestSuite()
    # adding test classes:
    test_suite.addTest(unittest.makeSuite(LoginToApp))
    test_suite.addTest(unittest.makeSuite(AddOrganisation))
    test_suite.addTest(unittest.makeSuite(AddUser))


    return test_suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(full_suite())