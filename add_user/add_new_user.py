import unittest
import time
import random

from globals import global_settings as globals
from helpers import operational_helpers as oh, functional_helpers as fh
from helpers.wrappers import screenshot_decorator


class AddUser(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.ef_driver = globals.init_driver()
        self.base_url = globals.base_url
        self.unique_id = str(time.time())[:10]
        self.pages = globals.Pages(self.ef_driver)
        self.user_email = f'karina.b+{self.unique_id}@weblinksoftware.com'
        self.foo_name = ['Eileen', 'Breena', 'Dallas', 'Omora', 'Ida',
                         'Moira', 'Tara Kelly', 'Nevina', 'Saraid', 'Meriel',
                         'Keara', 'Echna', 'Bevin']
        self.foo_lastname = ['Hayes', 'Dunne', 'Burke', 'Walsh', "O'Sullivan", 'McGrath', 'Collins', 'Brennan',
                             'Kavanagh',
                             'Murphy', 'Smith', 'Smith', 'Walsh']
        self.randoms_name = random.choice(self.foo_name)
        self.randoms_lastname = random.choice(self.foo_lastname)

    @classmethod
    def tearDownClass(self):
        self.ef_driver.quit()

    @screenshot_decorator
    def test_01_login_to_page(self):
        expected_title = 'Weblink StartApp'
        driver = self.ef_driver
        driver.maximize_window()

        fh.assert_title(driver, expected_title)

    def test_02_go_to_user_list(self):
        header_title_xpath = '//*[@id="container-3"]/content/user-list/div/div/div[1]/h2'
        expected_title = 'User List'
        driver = self.ef_driver

        self.pages.user.go_to_user_list()
        fh.assert_element_text(driver, header_title_xpath , expected_title)

    def test_03_add_new_user(self):
        new_user_email_xpath = f'//*[contains(text(), "{self.unique_id}")]'
        driver = self.ef_driver

        self.pages.user.invite_new_user(self.user_email, self.randoms_name, self.randoms_lastname)
        time.sleep(1)
        self.pages.user.search_new_user_in_filters(self.randoms_name, self.randoms_lastname)
        time.sleep(5)

        fh.found_items_in_list(driver, new_user_email_xpath)
