import unittest
import time

from globals import global_settings as globals
from helpers import operational_helpers as oh, functional_helpers as fh
from helpers.wrappers import screenshot_decorator


class LoginToApp(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.ef_driver = globals.init_driver()
        self.base_url = globals.base_url
        self.unique_id = str(time.time())[:10]
        self.pages = globals.Pages(self.ef_driver)

    @classmethod
    def tearDownClass(self):
        self.ef_driver.quit()

    @screenshot_decorator
    def test_01_login_to_page(self):
        expected_title = 'Weblink StartApp'
        driver = self.ef_driver
        driver.maximize_window()
        time.sleep(1)
        fh.assert_title(driver, expected_title)