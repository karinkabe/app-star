import time

import selenium
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import StaleElementReferenceException


def wait_for_elements(driver, xpath, max_seconds_to_wait=200, number_of_expected_elements=1):
    """Checking every second if list of elements under specified xpath was found

    :param driver: webdriver instance
    :param xpath: xpath of web element
    :param max_seconds_to_wait: maximum time in seconds to wait for element (default: 5)
    :param number_of_expected_elements: specifies minimum number of elements to be found
    :return: list of found elements
    """
    for seconds in range(max_seconds_to_wait):
        elements = driver.find_elements_by_xpath(xpath)

    print(f'Total waiting {seconds}s')

    if len(elements) >= number_of_expected_elements:
        return elements

    if seconds == (max_seconds_to_wait - 1):
        print('End of wait')
        assert len(elements) >= number_of_expected_elements, \
            f'Expected {number_of_expected_elements} elements but found {len(elements)} for xpath {xpath} in time of {max_seconds_to_wait}s'


def visibility_of_element_wait(driver: object, xpath: object, timeout: object = 100) -> object:
    """Checking if element specified by xpath is visible on page
    :param driver: webdriver instance
    :param xpath: xpath of web element
    :param timeout: time after looking for element will be stopped (default: 10)
    :return: first element in list of found elements
    """
    timeout_message = f"Element for xpath: '{xpath}' and url: {driver.current_url} not found in {timeout} seconds"
    locator = (By.XPATH, xpath)
    element_located = EC.visibility_of_element_located(locator)
    # wait = WebDriverWait(driver, timeout) # using EventFiringWebDriver
    wait = WebDriverWait(driver.wrapped_driver, timeout)  # using pure driver
    return wait.until(element_located, timeout_message)


def presence_of_element_wait(driver, xpath, timeout=200):
    """Checking if element specified by xpath is visible on page
    :param driver: webdriver instance
    :param xpath: xpath of web element
    :param timeout: time after looking for element will be stopped (default: 10)
    :return: first element in list of found elements
    """
    timeout_message = f"Element for xpath: '{xpath}' and url: {driver.current_url} not found in {timeout} seconds"
    locator = (By.XPATH, xpath)
    element_located = EC.presence_of_element_located(locator)
    wait = WebDriverWait(driver.wrapped_driver, timeout)
    return wait.until(element_located, timeout_message)


def clickability_of_element_wait(driver, xpath, timeout=200):
    """Checking if element specified by xpath is clickable on page
    :param driver: webdriver instance
    :param xpath: xpath of web element
    :param timeout: time after looking for element will be stopped (default: 10)
    :return: first element in list of found elements
    """
    timeout_message = f"Element for xpath: '{xpath}' and url: {driver.current_url} not found in {timeout} seconds"
    locator = (By.XPATH, xpath)
    element_located = EC.element_to_be_clickable(locator)
    wait = WebDriverWait(driver, timeout)
    return wait.until(element_located, timeout_message)


def try_to_click(element, xpath, driver, title='', number_tryies=20):
    """Checking if element specified by xpath is clicked on page
    :param element: name of veb element
    :param xpath: xpath of web element
    :param driver: webdriver instance
    :param title: expected text title of veb page
    :param number_tryies: the number of click attempts
    """
    should_stop = False
    for check in range(number_tryies):
        try:
            element.click()
        except StaleElementReferenceException:
            print(f'Stale Element Retrying times {check}')
            time.sleep(0.5)
            element = driver.find_element_by_xpath(xpath)
            if check == number_tryies - 1:
                print('Sorry cannot get this element')
                raise
        else:
            for check_title in range(3):
                if title != '':
                    print('Expected title after click: ', title, ', actual: ', driver.title)
                if driver.title == title or title == '':
                    should_stop = True
                    break
                time.sleep(0.5)

        if should_stop:
            break


def try_to_select(element, xpath, driver, select_number, title='', number_tryies=20):
    """Checking if element specified by xpath is selected on page

    :param element: name of veb element
    :param xpath: xpath of web element
    :param driver: webdriver instance
    :param select_number: the number of the element from the list
    :param title: expected text title of veb page
    :param number_tryies: the number of select attempts
    """
    should_stop = False
    for check in range(number_tryies):
        try:
            element.select_by_index(select_number)
        except StaleElementReferenceException:
            print(f'Stale Element Retrying times {check}')
            time.sleep(0.5)
            element = driver.find_element_by_xpath(xpath)
            if check == number_tryies - 1:
                print('Sorry cannot get this element')
                raise
        else:
            for check_title in range(3):
                if title != '':
                    print('Expected title after click: ', title, ', actual: ', driver.title)
                if driver.title == title or title == '':
                    should_stop = True
                    break
                time.sleep(0.5)

        if should_stop:
            break


def try_to_send_keys(element, xpath, driver, keys, title='', number_tryies=20):
    """Checking if element specified by xpath is get send message on page

    :param element: name of veb element
    :param xpath: xpath of web element
    :param driver: webdriver instance
    :param keys: meesage to send
    :param title: expected text title of veb page
    :param number_tryies: the number of send attempts
    """
    should_stop = False
    for check in range(number_tryies):
        try:
            element.send_keys(keys)
        except StaleElementReferenceException:
            print(f'Stale Element Retrying times {check}')
            time.sleep(0.5)
            element = driver.find_element_by_xpath(xpath)
            if check == number_tryies - 1:
                print('Sorry cannot get this element')
                raise
        else:
            for check_title in range(3):
                if title != '':
                    print('Expected title after click: ', title, ', actual: ', driver.title)
                if driver.title == title or title == '':
                    should_stop = True
                    break
                time.sleep(0.5)

        if should_stop:
            break


def invisibility_of_element_wait(driver, xpath, timeout=200):
    """Checking if element specified by xpath is invisible on page
    :param driver: webdriver instance
    :param xpath: xpath of web element
    :param timeout: time after looking for element will be stopped (default: 10)
    :return: first element in list of found elements
    """
    timeout_message = f"Element for xpath: '{xpath}' and url: {driver.current_url} found in {timeout} seconds but expecting to be invisible"
    locator = (By.XPATH, xpath)
    element_invisible = EC.visibility_of_element_located(locator)

    # wait = WebDriverWait(driver, timeout) # using EventFiringWebDriver
    wait = WebDriverWait(driver.wrapped_driver, timeout)  # using pure driver
    return wait.until_not(element_invisible, timeout_message)


def scroll_down(driver):
    """Scroll down to the bottom of the page

    :param driver: webdriver instance
    """
    # When test doesn't see element scroll down to bottom
    SCROLL_PAUSE_TIME = 0.5

    # Get scroll height
    last_height = driver.execute_script("return document.body.scrollHeight")

    for i in range(10):
        # Scroll down to bottom
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        # Wait to load page
        time.sleep(SCROLL_PAUSE_TIME)

        # Calculate new scroll height and compare with last scroll height
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height
    else:
        raise Exception('Scrolling down failed')


def wait_for_elements_to_be_invisible(driver, xpath, max_seconds_to_wait=5):
    """Checking every second if list of elements under specified xpath is empty

        :param driver: webdriver instance
        :param xpath: xpath of web element
        :param max_seconds_to_wait: maximum time in seconds to wait to empty list (default: 5)
        :return: True if element found
    """
    for seconds in range(max_seconds_to_wait):
        elements = driver.find_elements_by_xpath(xpath)

        print(f'Total waiting {seconds}s')

        if len(elements) > 0:
            return True

        if seconds == (max_seconds_to_wait - 1):
            print('End of wait')
            assert len(elements) == 0, \
                f'Expected 0 elements but found {len(elements)} for xpath {xpath} in time of {max_seconds_to_wait}s'

        time.sleep(1)
