import os


def project_path():
    """
    Running independent tests in inner folders cause mismatch in paths
    This function helps with this problem and allow to run tests independently or all together
    :return: path to root
    """

    # The endswith() method returns True if the string ends with the specified value, otherwise False
    # if os.getcwd().endswith("weblink-frommydoor-tests") or os.getcwd().endswith(
    #         # f"FMD-TEST-{os.environ['FMD_VERSION']}"):
    #         "FMD-TEST-tests"):
    #
    #     print(f'Using project path {os.getcwd()}')
    #     return os.getcwd()
    # else:
    #     project_root = f'{os.getcwd()}/..'
    #     print(f'Using path for individual tests {project_root}')
    #     return os.path.realpath(project_root)

    # The endswith() method returns True if the string ends with the specified value, otherwise False
    if os.getcwd().endswith("AppStar") or os.getcwd().endswith("App Star"):
            # or os.getcwd().endswith("FMD-TEST-tests") or os.getcwd().endswith("fmd"):

        print(f'Using project path {os.getcwd()}')
        return os.getcwd()
    else:
        project_root = f'{os.getcwd()}/..'
        print(f'Using path for individual tests {project_root}')
        return os.path.realpath(project_root)