import time
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import random

from helpers import operational_helpers as oh
from helpers.global_helper import project_path


class UserPage:
    def __init__(self, driver):
        self.user_list_xpath = f'//*[contains(text(), "User List")]'
        self.unique_id = str(time.time())[:10]
        self.driver = driver

    def go_to_user_list(self):
        # Click on the User list tab in left Menu
        time.sleep(3)
        user_list = oh.visibility_of_element_wait(self.driver, self.user_list_xpath, 200)
        time.sleep(3)
        user_list.click()

    def invite_new_user(self, email, first_name, last_name):
        add_button_xpath = '//*[@aria-label="Invite user"]'
        email_input_xpath = '//*[@id="mat-input-7"]'
        first_name_xpath = '//*[@id="mat-input-8"]'
        last_name_xpath = '//*[@id="mat-input-9"]'
        organisation_select_xpath = '//*[@id="mat-select-5"]/div/div[1]'
        big_team_org_xpath = f'//*[contains(text(), " Big Team ")]'
        role_select_xpath = '//*[@id="mat-dialog-0"]/app-invite/div/mat-dialog-content/div/form/div[5]/mat-form-field/div/div[1]'
        user_role_xpath = '(//mat-option)[1]'
        invite_button_xpath = '//button[@class="submit-button mat-raised-button mat-button-base mat-accent"]'
        ok_button_xpath = f'//span[contains(text(), " OK ")][1]'

        # Click plus icon on right side in header menu
        click_invite_user = oh.visibility_of_element_wait(self.driver, add_button_xpath, 100)
        click_invite_user.click()

        time.sleep(3)
        # Send email to email input
        send_email = oh.visibility_of_element_wait(self.driver, email_input_xpath, 100)
        send_email.send_keys(email)

        # Send First name to email input
        send_first_name = oh.visibility_of_element_wait(self.driver, first_name_xpath, 100)
        send_first_name.send_keys(first_name)

        # Send Last name to email input
        send_last_name = oh.visibility_of_element_wait(self.driver, last_name_xpath, 100)
        send_last_name.send_keys(last_name)

        # Click on Organisation select
        click_on_organisation_select= oh.visibility_of_element_wait(self.driver, organisation_select_xpath, 100)
        click_on_organisation_select.click()

        # Choose Big Team Organisation
        big_team_organisation = oh.visibility_of_element_wait(self.driver, big_team_org_xpath, 100)
        big_team_organisation.click()
        time.sleep(2)
        # Click on Role select
        click_on_role_select = oh.visibility_of_element_wait(self.driver, role_select_xpath, 100)
        click_on_role_select.click()

        time.sleep(1)
        # Choose User role
        user_role = oh.visibility_of_element_wait(self.driver, user_role_xpath, 100)
        time.sleep(2)
        user_role.click()
        time.sleep(2)
        # Click invite button
        invite_button = oh.visibility_of_element_wait(self.driver, invite_button_xpath, 100)
        invite_button.click()
        time.sleep(4)
        # Click ok button
        ok_button = oh.visibility_of_element_wait(self.driver, ok_button_xpath, 200)
        time.sleep(2)
        ok_button.click()


    def search_new_user_in_filters(self, name, last_name):
        user_first_name_search_xpath = '//*[@id="formly_5_input_firstName_0"]'
        user_last_name_search_xpath = '//*[@id="formly_5_input_lastName_1"]'
        user_role_search_xpath = '//*[@id="formly_5_select_role.name_2"]/div/div[1]'
        user_role_xpath = '//*[@id="mat-option-2"]/span'
        user_status_xpath = '//*[@id="formly_5_select_status_3"]/div/div[1]/span'
        pending_status_xpath = '//*[@id="mat-option-5"]/span'
        org_search_xpath = '//*[@id="formly_5_select_organisation.name_4"]/div/div[1]'
        big_team_org_xpath = '//*[@id="mat-option-14"]/span'
        apply_button_xpath = f'//*[contains(text(), " Apply ")]'

        # Send first name to Filters
        search_first_name = oh.visibility_of_element_wait(self.driver, user_first_name_search_xpath, 100)
        search_first_name.send_keys(name)
        # Send last name to Filters
        search_last_name = oh.visibility_of_element_wait(self.driver, user_last_name_search_xpath, 100)
        search_last_name.send_keys(last_name)

        time.sleep(2)
        # Select from User Role in filters - User
        user_role = oh.visibility_of_element_wait(self.driver, user_role_search_xpath, 100)
        user_role.click()
        time.sleep(2)
        # Choose User role
        user_role = oh.visibility_of_element_wait(self.driver, user_role_xpath, 100)
        user_role.click()
        time.sleep(1)
        # Select from User Status in filters - Pending
        user_status = oh.visibility_of_element_wait(self.driver, user_status_xpath, 100)
        user_status.click()
        time.sleep(1)
        select_pending_status = oh.visibility_of_element_wait(self.driver, pending_status_xpath, 100)
        select_pending_status.click()
        time.sleep(1)
        # Select from Organisation Name in filters - Big Team
        # org_name = oh.visibility_of_element_wait(self.driver, org_search_xpath, 100)
        # org_name.click()
        # time.sleep(1)
        # big_team_org = oh.visibility_of_element_wait(self.driver, big_team_org_xpath, 100)
        # big_team_org .click()
        time.sleep(1)
        # Click Apply button
        apply_button = oh.visibility_of_element_wait(self.driver, apply_button_xpath, 100)
        apply_button.click()