import time
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import random

from helpers import operational_helpers as oh
from helpers.global_helper import project_path


class OrganisationPage:
    def __init__(self, driver):
        self.organisation_list_xpath = f'//*[contains(text(), "Organisation List")]'
        self.unique_id = str(time.time())[:10]
        self.driver = driver

    def go_to_organisation_list(self):
        # Click on the User list tab in left Menu
        time.sleep(3)
        org_list = oh.visibility_of_element_wait(self.driver, self.organisation_list_xpath, 500)
        time.sleep(3)
        org_list.click()

    def add_new_organisation(self, full_name, short_name, address, vat_number, vat_tax_rate):
        plus_icon_xpath = '//*[@id="container-3"]/content/app-organisation-list/div/div/div[1]/button'
        org_full_name_input_xpath = '//*[@id="organisation_name"]'
        org_short_name_input_xpath = '//*[@id="organisation_shortName"]'
        org_type_xpath = '//*[@id="organisation_types"]/div/div[1]'
        banking_type_xpath = '//*[@id="mat-option-0"]/span'
        cloud_computing_xpath = '//*[@id="mat-option-1"]/span'
        address_org_xpath = '//*[@id="organisation_address"]'
        upload_file_xpath = '//*[@id="container-3"]/content/app-add-organisation/div/div/div[2]/app-formscontent/form/formly-form/formly-field/formly-group/formly-field[2]/app-forms-card-wrapper/div/div[2]/formly-group/formly-field/formly-wrapper-mat-form-field/mat-form-field/div/div[1]/div[2]/forms-type-upload/app-upload/div/div[2]/div/div/button'
        morze_img = f'{project_path()}/images/morze.jpg'
        billing_inforamtion_xpath = '//*[@id="organisation_organisationBillingId"]/label/div'
        vat_number_input_xpath = '//*[@id="billing_vatNumber"]'
        company_vat_tax_rate = '//*[@id="billing_vatTaxRate"]'
        company_currency = '//*[@id="container-3"]/content/app-add-organisation/div/div/div[2]/app-formscontent/form/formly-form/formly-field/formly-group/formly-field[3]/app-forms-card-wrapper/div/div[2]/formly-group/formly-field[4]/formly-wrapper-mat-form-field/mat-form-field/div/div[1]/div[2]'
        euro_select_xpath = f'//*[contains(text(), "Euro ")]'
        save_button_xpath = '//*[@id="container-3"]/content/app-add-organisation/div/div/div[2]/app-formscontent/form/button/span'
        time.sleep(2)

        # Click plus icon on right side in header menu
        click_plus_icon = oh.visibility_of_element_wait(self.driver, plus_icon_xpath, 100)
        click_plus_icon.click()
        time.sleep(3)
        # Click on Organisation type
        organisation_type = oh.visibility_of_element_wait(self.driver, org_type_xpath, 100)
        organisation_type.click()

        # Mark Banking type
        banking_type = oh.visibility_of_element_wait(self.driver, banking_type_xpath, 100)
        banking_type.click()

        # Mark Cloud Computing type
        cloud_computing = oh.visibility_of_element_wait(self.driver, cloud_computing_xpath, 100)
        cloud_computing.click()
        # Send Organisation Full Name
        org_full_name = oh.visibility_of_element_wait(self.driver, org_full_name_input_xpath, 100)
        org_full_name.send_keys(full_name)

        # Click to anywhere to close window with type of organisation
        action = ActionChains(self.driver)
        first_px = random.randint(250, 300)
        second_px = random.randint(100, 200)
        action.move_to_element_with_offset(org_full_name, first_px, second_px)
        action.click()
        action.perform()

        # Send Organisation Short Name
        org_short_name = oh.visibility_of_element_wait(self.driver, org_short_name_input_xpath, 100)
        org_short_name.send_keys(short_name)

        # Send Organisation Address
        org_address = oh.visibility_of_element_wait(self.driver, address_org_xpath, 100)
        org_address.send_keys(address)
        time.sleep(2)

        # # Upload Logo Company
        # upload_image_element = oh.presence_of_element_wait(self.driver, upload_file_xpath)
        # upload_image_element.send_keys(morze_img)
        # time.sleep(2)

        # Click Billing information
        billing_information = oh.visibility_of_element_wait(self.driver, billing_inforamtion_xpath, 100)
        billing_information.click()

        time.sleep(1)

        # Send Vat number
        vat_number_input = oh.visibility_of_element_wait(self.driver, vat_number_input_xpath, 100)
        vat_number_input.send_keys(vat_number)
        time.sleep(1)
        # Send Company Vat Tax Rate
        company_vat_tax_rate_input = oh.visibility_of_element_wait(self.driver, company_vat_tax_rate, 100)
        company_vat_tax_rate_input.send_keys(vat_tax_rate)
        time.sleep(1)
        # Send Company Currency
        company_currency_select = oh.visibility_of_element_wait(self.driver, company_currency, 100)
        company_currency_select.click()
        time.sleep(1)
        # Select Euro
        euro_select = oh.visibility_of_element_wait(self.driver, euro_select_xpath, 100)
        euro_select.click()
        time.sleep(1)
        # Click Save button
        save_button = oh.visibility_of_element_wait(self.driver, save_button_xpath, 100)
        save_button.click()

