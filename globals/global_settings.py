import os

from selenium import webdriver
from selenium.webdriver.support.events import EventFiringWebDriver
from helpers.screenshot_listener import ScreenshotListener
from helpers.global_helper import project_path
from helpers import functional_helpers as fh
from pages.user_page import UserPage
from pages.organisation_page import OrganisationPage

app_star_url = 'https://develop.d358ox72myxvpq.amplifyapp.com/'
base_url = app_star_url

user = 'karina.b+000@weblinksoftware.com'
password = 'qwerty123'

def init_driver(login=True):
    """
    Starting driver for all tests
    For machines different than local run on Windows (nt) is external WebDriver is expected under proper url
    :return:
    """
    if os.name == 'nt':
        path = project_path()
        driver = webdriver.Chrome(executable_path=rf"{path}/globals/chromedriver.exe")
    else:
        # To jenkins
        # print("Using settings for linux tests globals")
        # grid_webdriver = f'{ci_machine_url}:4445/wd/hub'
        # capabilities = webdriver.DesiredCapabilities.CHROME
        # driver = webdriver.Remote(command_executor=grid_webdriver,
        #                           desired_capabilities=capabilities)
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        driver = webdriver.Chrome(options=chrome_options)
        driver.set_window_size(1920, 1080)
    ef_driver = EventFiringWebDriver(driver, ScreenshotListener())

    if login:
        login_to_application(ef_driver)

    return ef_driver

def go_to_members_area(driver):
    # Go to members area from home page
    driver.get(base_url)
    go_to_members_area.click()

def login_to_application(driver):
    driver.get(base_url)
    fh.user_login(driver, user, password)

class Pages:
    def __init__(self, driver):
        self.user = UserPage(driver)
        self.org = OrganisationPage(driver)